#include "../internal.h"

#define roundup32(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))

YPC_EXPORT int ypc_val_resize(ypc_val *v, size_t s) {
    if(s >= UINT32_MAX/2)
        return -1;
    v->size = s;
    roundup32(v->size);
    uint8_t *buf = realloc(v->buf, v->size);
    if(!buf)
        return -1;
    v->buf = buf;
    return 0;
}

