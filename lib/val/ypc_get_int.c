#include "../internal.h"
#include "../util/byteswap.h"

YPC_EXPORT int64_t ypc_get_int(ypc_get g) {
    /* XXX: Casting from unsigned to signed is UB, so memcpy() instead. */
    switch(*g.buf) {
    case YPCT_INT8:
        {
            int8_t v;
            memcpy(&v, g.buf+1, 1);
            return v;
        }
    case YPCT_INT16:
        {
            uint16_t v;
            int16_t i;
            memcpy(&v, g.buf+1, 2);
            v = ypc__hn16(v);
            memcpy(&i, &v, 2);
            return i;
        }
    case YPCT_INT32:
        {
            uint32_t v;
            int32_t i;
            memcpy(&v, g.buf+1, 4);
            v = ypc__hn32(v);
            memcpy(&i, &v, 4);
            return i;
        }
    case YPCT_INT64:
        {
            uint64_t v;
            int64_t i;
            memcpy(&v, g.buf+1, 8);
            v = ypc__hn64(v);
            memcpy(&i, &v, 8);
            return i;
        }
    }
    assert(0 && "ypc_get_int() used on a non-integer value");
    return -1;
}
