#include "../internal.h"

YPC_EXPORT int ypc_add_bin(ypc_val *v, const uint8_t *buf, size_t len) {
    if(len >= 1<<24 || ypc__val_addsize(v, len+4) != 0)
        return -1;
    v->buf[v->len++] = 8;
    v->buf[v->len++] = len >> 16;
    v->buf[v->len++] = len >> 8;
    v->buf[v->len++] = len;
    memcpy(v->buf+v->len, buf, len);
    v->len += len;
    return 0;
}
