#include "../internal.h"
#include "../util/byteswap.h"

YPC_EXPORT int ypc_add_int(ypc_val *v, int64_t i) {
    if(ypc__val_addsize(v, 9) != 0)
        return -1;
    if(i >= INT8_MIN && i <= INT8_MAX) {
        v->buf[v->len++] = YPCT_INT8;
        v->buf[v->len++] = i;
    } else if(i >= INT16_MIN && i <= INT16_MAX) {
        v->buf[v->len++] = YPCT_INT16;
        uint16_t n = ypc__hn16(i);
        memcpy(v->buf+v->len, &n, 2);
        v->len += 2;
    } else if(i >= INT32_MIN && i <= INT32_MAX) {
        v->buf[v->len++] = YPCT_INT32;
        uint32_t n = ypc__hn32(i);
        memcpy(v->buf+v->len, &n, 4);
        v->len += 4;
    } else {
        v->buf[v->len++] = YPCT_INT64;
        uint64_t n = ypc__hn64(i);
        memcpy(v->buf+v->len, &n, 8);
        v->len += 8;
    }
    return 0;
}
