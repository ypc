#include "../internal.h"
#include "../util/byteswap.h"

YPC_EXPORT double ypc_get_float(ypc_get g) {
    assert(*g.buf == YPCT_FLOAT && "ypc_get_float() used on a non-float value");
    uint64_t u;
    double v;
    memcpy(&u, g.buf+1, 8);
    u = ypc__hn64(u);
    memcpy(&v, &u, 8);
    return v;
}
