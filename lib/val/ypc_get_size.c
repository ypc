#include "../internal.h"

YPC_EXPORT size_t ypc_get_size(ypc_get g) {
    switch(*g.buf) {
    case YPCT_NULL:
    case YPCT_FALSE:
    case YPCT_TRUE:
    case YPCT_CLOSE:
        return 1;
    case YPCT_INT8:
        return 2;
    case YPCT_INT16:
        return 3;
    case YPCT_INT32:
        return 5;
    case YPCT_INT64:
    case YPCT_FLOAT:
        return 9;
    case YPCT_BIN:
        return 4+ypc_get_bin_len(g);
    case YPCT_TEXT:
        return 2+strlen((const char *)g.buf+1);
    case YPCT_ARRAY:
    case YPCT_MAP:
        {
            size_t l = 1;
            while(g.buf[l] != YPCT_CLOSE) {
                ypc_get v = {g.buf+l};
                l += ypc_get_size(v);
            }
            return l+1;
        }
    }
    assert(0 && "ypc_get_size() used on an invalid value");
    return 0;
}
