#include "../internal.h"
#include "../util/byteswap.h"

YPC_EXPORT int ypc_add_float(ypc_val *v, double d) {
    if(ypc__val_addsize(v, 9))
        return -1;
    v->buf[v->len++] = 7;

    uint64_t n;
    memcpy(&n, &d, 8);
    n = ypc__hn64(n);
    memcpy(v->buf+v->len, &n, 8);
    v->len += 8;
    return 0;
}
