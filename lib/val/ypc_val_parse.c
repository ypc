#include "../internal.h"
#include "../util/utf8.h"

typedef enum {
    YPCP_TYPE, /* Expecting a type byte */
    YPCP_SKIP, /* Skipping over 'len' bytes */
    YPCP_BIN1, /* Expecting first length byte of binary string */
    YPCP_BIN2, /* ^ second */
    YPCP_BIN3, /* ^ third */
    YPCP_TEXT, /* Expecting UTF-8 text */
} ypc__parse_state;


static int ypc__parse_endval(ypc_val_parser *p) {
    p->state = YPCP_TYPE;
    return p->depth == 0;
}


static int ypc__parse_type(ypc_val_parser *p, uint8_t type) {
    if(p->depth && p->bitmap & 1 && !p->wantval) {
        if(!(type == YPCT_INT8 || type == YPCT_INT16 || type == YPCT_INT32 || type == YPCT_INT64
                    || type == YPCT_BIN || type == YPCT_TEXT || type == YPCT_CLOSE))
            return -1;
        p->wantval = true;
    } else if(p->wantval)
        p->wantval = false;

    switch(type) {

    case YPCT_NULL:
    case YPCT_FALSE:
    case YPCT_TRUE:
        return ypc__parse_endval(p);

    case YPCT_INT8:
        p->len = 1;
        p->state = YPCP_SKIP;
        return 0;

    case YPCT_INT16:
        p->len = 2;
        p->state = YPCP_SKIP;
        return 0;

    case YPCT_INT32:
        p->len = 4;
        p->state = YPCP_SKIP;
        return 0;

    case YPCT_INT64:
    case YPCT_FLOAT:
        p->len = 8;
        p->state = YPCP_SKIP;
        return 0;

    case YPCT_BIN:
        p->state = YPCP_BIN1;
        return 0;

    case YPCT_TEXT:
        p->state = YPCP_TEXT;
        return 0;

    case YPCT_ARRAY:
    case YPCT_MAP:
        if(p->depth == 32)
            return -1;
        p->depth++;
        p->bitmap <<= 1;
        if(type == YPCT_MAP)
            p->bitmap |= 1;
        p->state = YPCP_TYPE;
        return 0;

    case YPCT_CLOSE:
        if(!p->depth)
            return -1;
        p->bitmap >>= 1;
        p->depth--;
        return ypc__parse_endval(p);
    }
    return -1;
}

YPC_EXPORT int ypc_val_parse(ypc_val_parser *p, const uint8_t *buf, size_t *lenp) {
    size_t len = *lenp;
    int r = 0;

    while(r == 0 && len) {

        switch(p->state) {

        case YPCP_TYPE:
            r = ypc__parse_type(p, *buf);
            break;

        case YPCP_SKIP:
            {
                size_t skip = p->len > len ? len : p->len;
                /* -1 because the outer loop we're in already 'consumes' one byte */
                len -= skip-1;
                buf += skip-1;
                p->len -= skip;
            }
            if(!p->len)
                r = ypc__parse_endval(p);
            break;

        case YPCP_BIN1:
            p->len = (uint32_t)*buf << 16;
            p->state = YPCP_BIN2;
            break;

        case YPCP_BIN2:
            p->len |= (uint32_t)*buf << 8;
            p->state = YPCP_BIN3;
            break;

        case YPCP_BIN3:
            p->len |= (uint32_t)*buf;
            if(p->len)
                p->state = YPCP_SKIP;
            else
                r = ypc__parse_endval(p);
            break;

        case YPCP_TEXT:
            {
                uint32_t codepoint;
                p->utf8state = ypc__utf8_decode(p->utf8state, &codepoint, *buf);
                if(p->utf8state == UTF8_REJECT)
                    r = -1;
                else if(*buf == 0)
                    r = p->utf8state == UTF8_ACCEPT ? ypc__parse_endval(p) : -1;
            }
            break;
        }

        len--;
        buf++;
    }

    *lenp -= len;
    return r;
}


YPC_EXPORT void ypc_val_parse_init(ypc_val_parser *p) {
    p->bitmap = 0;
    p->len = 0;
    p->depth = 0;
    p->wantval = 0;
    p->state = YPCP_TYPE;
    p->utf8state = UTF8_ACCEPT;
}

