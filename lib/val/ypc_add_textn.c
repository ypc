#include "../internal.h"

YPC_EXPORT int ypc_add_textn(ypc_val *v, const char *s, size_t len) {
    if(len > len+2 || ypc__val_addsize(v, len+2) != 0)
        return -1;
    v->buf[v->len++] = 9;
    memcpy(v->buf+v->len, s, len);
    v->len += len;
    v->buf[v->len++] = 0;
    return 0;
}
