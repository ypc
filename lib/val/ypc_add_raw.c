#include "../internal.h"

YPC_EXPORT int ypc_add_raw(ypc_val *v, const uint8_t *buf, size_t len) {
    if(ypc__val_addsize(v, len) != 0)
        return -1;
    memcpy(v->buf+v->len, buf, len);
    v->len += len;
    return 0;
}
