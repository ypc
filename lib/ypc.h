#ifndef YPC_H_
#define YPC_H_

#ifdef YPC_INTERNAL_H_
#define YPC_IMPORT YPC_EXPORT
#elif defined(_WIN32) || defined(__CYGWIN__)
#define YPC_IMPORT __declspec(dllimport)
#else
#define YPC_IMPORT
#endif

#if defined(_MSC_VER) && !defined(__cplusplus) && !defined(inline)
#define inline _inline
#endif


#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct ypc ypc;
typedef struct ypc_serv ypc_serv;


typedef struct {
    uint8_t *buf;
    uint32_t size, len;
} ypc_val;

typedef struct {
    const uint8_t *buf;
} ypc_get;

typedef struct {
    const uint8_t *buf;
} ypc_iter;

typedef enum {
    YPC_NULL,
    YPC_BOOL,
    YPC_INT,
    YPC_FLOAT,
    YPC_BIN,
    YPC_TEXT,
    YPC_ARRAY,
    YPC_MAP,
    YPC_CLOSE
} ypc_type;


#define YPC_NONBLOCK  0x01
#define YPC_RECV      0x02
#define YPC_CANREAD   0x04
#define YPC_CANWRITE  0x08
#define YPC_COPY      0x10


#define YPC_EPROTOCOL      -10 /* Received an invalid message */
#define YPC_ECONNREFUSED    -9 /* Connection refused */
#define YPC_EINVAL          -8 /* Invalid argument */
#define YPC_EADDR           -7 /* Invalid or unrecognized address */
#define YPC_EPERM           -6 /* Not enough permissions */
#define YPC_EDISCONNECT     -5 /* Remote end disconnected */
#define YPC_EADDRINUSE      -4 /* Address already in use */
#define YPC_ENOSUPPORT      -3 /* Protocol/operation not supported */
#define YPC_ENOMEM          -2 /* Out of memory */
#define YPC_EFAIL           -1 /* Generic fail code, for when no specific error is available or useful */


/* I don't like having this struct part of the public ABI, but it is the kind
 * of struct one might want to embed. Let's just pray that 4x32bit will suffice
 * for eternity. */
typedef struct {
    uint32_t len;
    uint32_t bitmap;
    uint8_t utf8state; /* Can be reduced to 7 bits (or less, 4 least significant bits are always 0) */
    uint8_t depth;     /* Can be reduced to 6 bits */
    uint8_t state;     /* Can be reduced to 3 bits */
    uint8_t wantval;   /* Can be reduced to 1 bit */
    uint32_t pad;
} ypc_val_parser;


#ifdef __cplusplus
extern "C" {
#endif

YPC_IMPORT int ypc_init();

YPC_IMPORT void ypc_val_parse_init(ypc_val_parser *);
YPC_IMPORT int ypc_val_parse(ypc_val_parser *, const uint8_t *, size_t *);
YPC_IMPORT int ypc_val_resize(ypc_val *, size_t);
YPC_IMPORT int ypc_add_raw(ypc_val *, const uint8_t *, size_t);
YPC_IMPORT int ypc_add_int(ypc_val *, int64_t);
YPC_IMPORT int ypc_add_float(ypc_val *, double);
YPC_IMPORT int ypc_add_bin(ypc_val *, const uint8_t *, size_t);
YPC_IMPORT int ypc_add_textn(ypc_val *, const char *, size_t);
YPC_IMPORT int64_t ypc_get_int(ypc_get);
YPC_IMPORT double ypc_get_float(ypc_get);
YPC_IMPORT size_t ypc_get_size(ypc_get);

YPC_IMPORT int ypc_connect(ypc **, const char *);
YPC_IMPORT int ypc_process(ypc *, uint32_t);
YPC_IMPORT bool ypc_want_write(ypc *);
YPC_IMPORT int ypc_fd(ypc *);
YPC_IMPORT int ypc_send(ypc *, ypc_val *, uint32_t);
YPC_IMPORT int ypc_recv(ypc *, ypc_val *);
YPC_IMPORT void ypc_free(ypc *);

YPC_IMPORT int ypc_serv_unix(ypc_serv **, const char *);
YPC_IMPORT int ypc_serv_tcp(ypc_serv **, const char *);
YPC_IMPORT int ypc_serv_keypair(char *, size_t, char *, size_t);
YPC_IMPORT int ypc_serv_tcps(ypc_serv **, const char *, const char *, const char *);
YPC_IMPORT int ypc_serv_accept(ypc_serv *, ypc **, uint32_t);
YPC_IMPORT int ypc_serv_fd(ypc_serv *);
YPC_IMPORT void ypc_serv_free(ypc_serv *);

#ifdef __cplusplus
}
#endif


/* Smallish stuff that is better suited for inlining */
static inline int ypc_val_init(ypc_val *v, size_t size)   { v->buf = NULL; v->len = 0; return ypc_val_resize(v, size ? size : 512); }
static inline void ypc_val_free(ypc_val *v)               { free(v->buf); }
static inline int ypc_add_null(ypc_val *v)                { return ypc_add_raw(v, (const uint8_t *)"\0", 1); }
static inline int ypc_add_bool(ypc_val *v, bool b)        { return ypc_add_raw(v, (const uint8_t *)(b ? "\2" : "\1"), 1); }
static inline int ypc_add_text(ypc_val *v, const char *t) { return ypc_add_textn(v, t, strlen(t)); }
static inline int ypc_add_array(ypc_val *v)               { return ypc_add_raw(v, (const uint8_t *)"\12", 1); }
static inline int ypc_add_map(ypc_val *v)                 { return ypc_add_raw(v, (const uint8_t *)"\13", 1); }
static inline int ypc_add_close(ypc_val *v)               { return ypc_add_raw(v, (const uint8_t *)"\14", 1); }
static inline int ypc_add_val(ypc_val *v, ypc_get g)      { return ypc_add_raw(v, g.buf, ypc_get_size(g)); }

static inline ypc_get        ypc_val_get(ypc_val *v)    { ypc_get g = {v->buf}; return g; }
static inline bool           ypc_get_bool(ypc_get g)    { return *g.buf == 2; }
static inline const char *   ypc_get_text(ypc_get g)    { return (const char *)g.buf+1; }
static inline const uint8_t *ypc_get_bin(ypc_get g)     { return g.buf+4; }
static inline size_t         ypc_get_bin_len(ypc_get g) { return (g.buf[1] << 16) | (g.buf[2] << 8) | g.buf[3]; }
static inline ypc_iter       ypc_get_iter(ypc_get g)    { ypc_iter i = {g.buf+1}; return i; }
static inline bool           ypc_iter_end(ypc_iter i)   { return *i.buf == 12; }
static inline ypc_get        ypc_iter_next(ypc_iter *i) { ypc_get g = {i->buf}; i->buf += ypc_get_size(g); return g; }

/* While not very small, this is still a static inline to enable a bunch of
 * optimizations. E.g. ypc_get_type(v) == YPC_TEXT should compile down to
 * *v->buf == 9, which is much cheaper than a function call. A similar
 * optimization is possible when using ypc_get_type() in a switch statement.
 */
static inline ypc_type ypc_get_type(ypc_get v) {
    switch(*v.buf) {
    case 0:
        return YPC_NULL;
    case 1:
    case 2:
        return YPC_BOOL;
    case 3:
    case 4:
    case 5:
    case 6:
        return YPC_INT;
    case 7:
        return YPC_FLOAT;
    case 8:
        return YPC_BIN;
    case 9:
        return YPC_TEXT;
    case 10:
        return YPC_ARRAY;
    case 11:
        return YPC_MAP;
    case 12:
        return YPC_CLOSE;
    }
    return 0; /* Should not happen... */
}

#endif
