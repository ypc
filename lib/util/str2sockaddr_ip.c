#include "str2sockaddr.h"


int ypc__str2sockaddr_ip(const char **strp, ypc__saddr *addr) {
    const char *str = *strp;
    char ip[48];

    if(*str == '[') {
        str++;
        const char *ipend = strchr(str, ']');
        if(!ipend)
            return -1;
        int r = snprintf(ip, sizeof(ip), "%.*s", (int)(ipend-str), str);
        if(r < 0 || r >= (int)sizeof(ip))
            return -1;
        addr->ip6.sin6_family = AF_INET6;
        if(inet_pton(AF_INET6, ip, &addr->ip6.sin6_addr) != 1)
            return -1;
        str = ipend+1;
    } else {
        size_t iplen = strspn(str, "0123456789.");
        int r = snprintf(ip, sizeof(ip), "%.*s", (int)iplen, str);
        if(r <= 0 || r >= (int)sizeof(ip))
            return -1;
        addr->ip4.sin_family = AF_INET;
        if(inet_pton(AF_INET, ip, &addr->ip4.sin_addr) != 1)
            return -1;
        str += iplen;
    }

    *strp = str;
    return 0;
}
