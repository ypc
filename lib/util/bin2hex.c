#include <stdlib.h>
#include <stdint.h>

void ypc__bin2hex(char *hex, const uint8_t *bin, size_t bin_len) {
    static const char ch[] = "0123456789abcdef";
    while(bin_len) {
        hex[0] = ch[(*bin >> 4) & 0x0F];
        hex[1] = ch[(*bin >> 0) & 0x0F];
        hex += 2;
        bin++;
        bin_len--;
    }
    *hex = 0;
}
