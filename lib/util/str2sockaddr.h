#ifndef YPC_STR2SOCKADDR_H_
#define YPC_STR2SOCKADDR_H_

#include "../internal.h"

typedef union {
    struct sockaddr_in ip4;
    struct sockaddr_in6 ip6;
#ifndef WINSOCK
    struct sockaddr_un un;
#endif
    struct sockaddr addr;
} ypc__saddr;


/* Parse a 'unix:' or 'tcps://' string. */
int ypc__str2sockaddr(const char *, ypc__saddr *, const char **);

/* Parse the IPv4 or [IPv6] address portion at the start of the given string,
 * and write to the family and address fields of *addr.
 * Updates the string pointer to point to the byte after the IP address. */
int ypc__str2sockaddr_ip(const char **, ypc__saddr *);

/* Similar to above, but parses the ':port' part. */
int ypc__str2sockaddr_port(const char **, ypc__saddr *);

#endif
