#ifndef YPC_LIBSODIUM

#if defined(_MSC_VER)
#include <windows.h>

int ypc__randombytes(unsigned char *data, unsigned int len) {
    static HCRYPTPROV prov;

    if(!CryptAcquireContext(&prov, NULL, NULL, PROV_RSA_FULL, 0))
        return -1;

    int r = 0;
    if(!CryptGenRandom(prov, len, data))
        r = -1;

    CryptReleaseContext(prov, 0);
    return r;
}


#else /* POSIX */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int ypc__randombytes(unsigned char *data, unsigned int len) {
    /* open/close /dev/urandom at every call. It's not terribly efficient, but
     * I favour thread-safety over performance here.
     * This implementation is based on randombytes/devurandom.c in NaCl
     * 20110221, major difference is the open/close and error handling.
     */

    int fd = open("/dev/urandom", O_RDONLY);
    if(fd < 0)
        return -1;

    while(len) {
        ssize_t i = len > 1048576 ? 1048576 : len;
        i = read(fd, data, i);
        if(i < 1) {
            close(fd);
            return -1;
        }
        data += i;
        len -= i;
    }

    close(fd);
    return 0;
}

#endif

#endif
