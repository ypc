#include <stdlib.h>
#include <stdint.h>

#define inrange(c, f, t) (((unsigned int)(c))-(f) <= (t)-(f))
#define ishex(c) (inrange(c, '0', '9') || inrange(c|0x20, 'a', 'f'))
#define hexval(c) (inrange(c, '0', '9') ? (c)-'0' : (c|0x20)+10-'a')

int ypc__hex2bin(uint8_t *bin, const char *msg, size_t bin_len) {
    while(ishex(msg[0]) && ishex(msg[1]) && bin_len) {
        *bin = (hexval(msg[0]) << 4) | hexval(msg[1]);
        msg += 2;
        bin_len--;
        bin++;
    }
    return bin_len || *msg ? -1 : 0;
}
