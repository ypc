#ifndef YPC_TWEETNACL_H
#define YPC_TWEETNACL_H

#include <stdint.h>

/* Rename symbols to avoid conflicts when static linking. */
#define crypto_box_keypair       ypc__crypto_box_keypair
#define crypto_box_beforenm      ypc__crypto_box_beforenm
#define crypto_box_afternm       ypc__crypto_box_afternm
#define crypto_box_open_afternm  ypc__crypto_box_open_afternm
#define crypto_box               ypc__crypto_box
#define crypto_box_open          ypc__crypto_box_open

int crypto_box_keypair(uint8_t *, uint8_t *);
int crypto_box_beforenm(uint8_t *, const uint8_t *, const uint8_t *);
int crypto_box_afternm(uint8_t *, const uint8_t *, unsigned long long, const uint8_t *, const uint8_t *);
int crypto_box_open_afternm(uint8_t *, const uint8_t *, unsigned long long, const uint8_t *, const uint8_t *);
int crypto_box(uint8_t *, const uint8_t *, unsigned long long, const uint8_t *, const uint8_t *, const uint8_t *);
int crypto_box_open(uint8_t *, const uint8_t *, unsigned long long, const uint8_t *, const uint8_t *, const uint8_t *);

#endif
