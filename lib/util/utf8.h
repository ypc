/* Copyright (c) 2008-2010 Bjoern Hoehrmann <bjoern@hoehrmann.de>
 * See http://bjoern.hoehrmann.de/utf-8/decoder/dfa/ for details.
 *
 * Minor modifications for ypc:
 * - Split into .c/.h files
 * - Renamed global symbols
 * - Changed state argument to be a scalar value rather than a pointer
 */

#include <stdint.h>

#define UTF8_ACCEPT 0
#define UTF8_REJECT 12

extern const uint8_t ypc__utf8d[];

static inline uint32_t ypc__utf8_decode(uint32_t state, uint32_t* codep, uint32_t byte) {
    uint32_t type = ypc__utf8d[byte];

    *codep = (state != UTF8_ACCEPT) ?
        (byte & 0x3fu) | (*codep << 6) :
        (0xff >> type) & (byte);

    return ypc__utf8d[256 + state + type];
}

