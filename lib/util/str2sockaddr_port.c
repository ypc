#include "str2sockaddr.h"

#include <string.h>
#include <stdio.h>

/* Similar to str2sockaddr_ip, but parses the port (after ':'). */
int ypc__str2sockaddr_port(const char **strp, ypc__saddr *addr) {
    const char *str = *strp;

    uint32_t res = 0;
    while(((unsigned int)*str) - '0' <= 9 && res < 65536) {
        if(!res && *str == '0')
            return -1;
        res = (res * 10) + *str - '0';
        str++;
    }
    if(!res || res >= 65536)
        return -1;

    if(addr->addr.sa_family == AF_INET)
        addr->ip4.sin_port = htons(res);
    else
        addr->ip6.sin6_port = htons(res);

    *strp = str;
    return 0;
}
