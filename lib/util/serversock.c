#include "../internal.h"

int ypc__serversock(int domain, void *addr, socklen_t addrlen, bool dualstack) {
    int fd = ypc__socket(domain, SOCK_STREAM, 0);
    if(fd < 0)
        return fd;

    int val = 1;
    if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(int)) < 0) {
        val = ypc__errcode(0);
        close(fd);
        return val;
    }

#ifdef IPV6_V6ONLY
    if(dualstack) {
        val = 0;
        if(setsockopt(fd, IPPROTO_IPV6, IPV6_V6ONLY, &val, sizeof(int)) < 0) {
            val = ypc__errcode(0);
            close(fd);
            return val;
        }
    }
#endif

    if(bind(fd, addr, addrlen) < 0 || listen(fd, 64) < 0) {
        val = ypc__errcode(0);
        close(fd);
        return val;
    }
    return fd;
}
