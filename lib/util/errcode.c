#include "../internal.h"

int ypc__errcode(int e) {
    switch(e ? e : ypc__neterrno) {

    case ypc__neterrcode(EADDRINUSE):
        return YPC_EADDRINUSE;

    case ypc__neterrcode(EACCES):
        return YPC_EPERM;

    case ypc__neterrcode(ECONNREFUSED):
        return YPC_ECONNREFUSED;

#ifdef WINSOCK
    case ypc__neterrcode(_NOT_ENOUGH_MEMORY):
#else
    case ypc__neterrcode(ENOMEM):
#endif
        return YPC_ENOMEM;

    case ypc__neterrcode(EOPNOTSUPP):
        return YPC_ENOSUPPORT;

#ifndef WINSOCK
    case ypc__neterrcode(EPIPE):
#endif
    case ypc__neterrcode(ECONNRESET):
    case ypc__neterrcode(ENETRESET):
        return YPC_EDISCONNECT;

    }
    return YPC_EFAIL;
}
