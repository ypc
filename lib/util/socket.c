#include "../internal.h"

int ypc__socket(int d, int t, int p) {
    int fd = socket(d, t, p);
    if(fd < 0)
        return ypc__errcode(0);
    int r;

#ifdef WINSOCK
    unsigned long on = 1;
    r = ioctlsocket(fd, FIONBIO, &on);	

#else
    r = fcntl(fd, F_GETFL);
    if(r < 0)
        r = fcntl(fd, F_SETFL, r|O_NONBLOCK);
#endif

    if(r < 0) {
        r = ypc__errcode(0);
        close(fd);
        return r;
    }
    return fd;
}
