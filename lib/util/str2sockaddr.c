#include "str2sockaddr.h"

int ypc__str2sockaddr(const char *str, ypc__saddr *addr, const char **key) {
    memset(addr, 0, sizeof(ypc__saddr));

    if(strncmp(str, "unix:", 5) == 0 && str[5]) {
#ifdef WINSOCK
        return YPC_ENOSUPPORT;
#else
        addr->un.sun_family = AF_UNIX;
        int r = snprintf(addr->un.sun_path, sizeof(addr->un.sun_path), "%s", str+5);
        if(r < 0 || r >= (int)sizeof(addr->un.sun_path))
            return YPC_EADDR;
        return 0;
#endif
    }

    int tcp = strncmp(str, "tcp://", 6) == 0 && str[6];
    int tcps = strncmp(str, "tcps://", 7) == 0 && str[7];
    if(tcp || tcps) {
        str += tcp ? 6 : 7;
        if(ypc__str2sockaddr_ip(&str, addr) < 0)
            return YPC_EADDR;
        if(*str != ':')
            return YPC_EADDR;
        str++;
        if(ypc__str2sockaddr_port(&str, addr) < 0)
            return YPC_EADDR;

        if(tcps) {
            if(*str != '/')
                return YPC_EADDR;
            *key = ++str;
        } else {
            if(*str == '/')
                str++;
            if(*str)
                return YPC_EADDR;
        }

        return 0;
    }

    return YPC_EADDR;
}
