#include "internal.h"

YPC_EXPORT int ypc_init() {
#ifdef YPC_LIBSODIUM
    if(sodium_init() == -1)
        return YPC_EFAIL;
#endif

#ifdef WINSOCK
    /* XXX: If there's ever going to be a ypc_deinit() function, we have to
     * make sure that WSAStartup() is called at most once, because it keeps a
     * reference counter. */
    struct WSAData arg;
    if(WSAStartup(MAKEWORD(2, 2), &arg) < 0)
        return YPC_EFAIL;
#endif

    return 0;
}
