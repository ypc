#ifndef YPC_INTERNAL_H_
#define YPC_INTERNAL_H_

#if defined(_WIN32) || defined(__MINGW32__) || defined(__CYGWIN__)
#define YPC_EXPORT __declspec(dllexport)
#else
#define YPC_EXPORT
#endif

#include "ypc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>


#if defined(_MSC_VER) || defined(__MINGW32__)
#define WINSOCK
#include <winsock2.h>
#include <ws2tcpip.h>
#define snprintf _snprintf
typedef int ssize_t;
/* We don't use close() on non-sockets on windows, so this hack should be safe. */
#define close closesocket
#define WSAEAGAIN WSAEWOULDBLOCK
#define ypc__neterrcode(n) (WSA##n)
#define ypc__neterrno WSAGetLastError()
#define ypc__neterr(n) (WSAGetLastError() == WSA##n)

#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/uio.h>
#include <sys/un.h>
#define ypc__neterrcode(n) (n)
#define ypc__neterrno errno
#define ypc__neterr(n) (errno == n)
#endif


#if defined(__MINGW32__)
/* MinGW header files aren't complete... */
WINSOCK_API_LINKAGE const char WSAAPI inet_pton(int af, const char *src, void *dst);
#endif


/* Wrapper for accessing struct iovec / WSABUF */
#ifdef WINSOCK
typedef WSABUF ypc_iovec;
#define ypc__iovec_buf(x) ((x).buf)
#define ypc__iovec_len(x) ((x).len)
#else
typedef struct iovec ypc_iovec;
#define ypc__iovec_buf(x) ((x).iov_base)
#define ypc__iovec_len(x) ((x).iov_len)
#endif


#ifdef YPC_LIBSODIUM
#include <sodium.h>
#else
#include "util/tweetnacl.h"
#endif


#define YPC_TCPS_MAXPACKET 4096


#define YPCT_NULL    0
#define YPCT_FALSE   1
#define YPCT_TRUE    2
#define YPCT_INT8    3
#define YPCT_INT16   4
#define YPCT_INT32   5
#define YPCT_INT64   6
#define YPCT_FLOAT   7
#define YPCT_BIN     8
#define YPCT_TEXT    9
#define YPCT_ARRAY  10
#define YPCT_MAP    11
#define YPCT_CLOSE  12


typedef enum {
    YPCN_CONNECTING,
    YPCN_TCPS,       /* tcps:// handshake */
    YPCN_CONNECTED,
    YPCN_DISCON
} ypc_state;


typedef struct ypc_msg ypc_msg;
struct ypc_msg {
    /* Interior pointers, used in either the send or receive queue. */
    ypc_msg *next, *prev;
    ypc_val v;
};


typedef struct {
    ypc_msg *first, *last;
} ypc_queue;


typedef struct {
    size_t slen; /* including first byte */
    size_t soff;
    size_t rlen; /* excluding first byte */
    size_t roff;

    /* Our own ephemeral public/secret keys */
    uint8_t sk[32];
    uint8_t pk[32];

    /* Server permanent public/secret key. */
    uint8_t ssk[32];
    uint8_t spk[32];

    /* The crypto_box_beforenm() state used for sending and receiving. */
    uint8_t nm[32];

    uint8_t snonce[24];
    uint8_t rnonce[24];

    /* The 16 bytes before the message is used for the crypto_box() zero bytes,
     * these are not actually sent over the network. One byte before the actual
     * send[] message is also reserved for the length prefix, which IS sent
     * over the network. */
    uint8_t send[16+YPC_TCPS_MAXPACKET];
    uint8_t recv[16+YPC_TCPS_MAXPACKET];
} ypc_tcps;


struct ypc {
    int fd;
    ypc_state state;
    bool server;

    ypc_queue send;
    size_t send_off;

    ypc_queue recv;
    ypc_val recv_val;
    ypc_val_parser recv_parse;

    ypc_tcps *tcps;
};


struct ypc_serv {
    int fd;
    bool secure;
    uint8_t ssk[32];
    uint8_t spk[32];
};


/* queue.c */
ypc_msg *ypc__dequeue(ypc_queue *);
void ypc__enqueue(ypc_queue *, ypc_msg *);

/* msgio.c */
int ypc__msgio_recv(ypc *, uint8_t *, size_t);
int ypc__msgio_sendbuf(ypc *, ypc_iovec *, int);
void ypc__msgio_sendlen(ypc *, size_t);

/* tcps.c */
int ypc__tcps_init(ypc *);
int ypc__tcps_recv(ypc *, uint8_t *, size_t);
int ypc__tcps_sendbuf(ypc *, ypc_iovec *, int);
void ypc__tcps_sendlen(ypc *, size_t);

/* util/errcode.c */
int ypc__errcode(int);

/* util/socket.c */
int ypc__socket(int, int, int);
/* util/serversock.c */
int ypc__serversock(int, void *, socklen_t, bool);
/* util/bin2hex.c */
void ypc__bin2hex(char *, const uint8_t *, size_t);
/* util/hex2bin.c */
int ypc__hex2bin(uint8_t *, const char *, size_t);


static inline int ypc__val_addsize(ypc_val *v, size_t s) {
    size_t want = v->len + s;
    if(v->len > want)
        return -1;
    if(want > v->size)
        return ypc_val_resize(v, want);
    return 0;
}

#endif
