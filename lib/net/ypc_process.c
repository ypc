#include "../internal.h"

/* Size of the read buffer. Beware that this buffer is allocated on the stack. */
#define YPC_READ_BUF_SIZE (8*1024)


static int ypc__select(ypc *y, uint32_t *flags) {
    if(*flags & (YPC_NONBLOCK|YPC_CANREAD|YPC_CANWRITE))
        return 0;

    bool wantwrite = ypc_want_write(y);
    fd_set rd, wr, *wrp = NULL, *exp = NULL;
    FD_ZERO(&rd);
    FD_ZERO(&wr);
    FD_SET(y->fd, &rd);

    if(wantwrite) {
        FD_SET(y->fd, &wr);
        wrp = &wr;
    }
#ifdef WINSOCK
    fd_set ex;
    FD_ZERO(&ex);
    /* Winsock doesn't report read/write state if the connect() failed, it only
     * reports an exception. */
    if(y->state == YPCN_CONNECTING) {
        FD_SET(y->fd, &ex);
        exp = &ex;
    }
#endif

    int r;
    do {
        r = select(y->fd+1, &rd, wrp, exp, NULL);
    } while(r < 0 && ypc__neterr(EINTR));
    if(r < 0)
        return ypc__errcode(0);

    if(FD_ISSET(y->fd, &rd))
        *flags |= YPC_CANREAD;
    if(FD_ISSET(y->fd, &wr))
        *flags |= YPC_CANWRITE;
#ifdef WINSOCK
    if(FD_ISSET(y->fd, &ex))
        *flags |= YPC_CANWRITE;
#endif

    return 0;
}


static int ypc__connecting(ypc *y) {
    int err;
    socklen_t len = sizeof(int);
    int r = getsockopt(y->fd, SOL_SOCKET, SO_ERROR, &err, &len);
    if(r < 0)
        return ypc__errcode(0);

    if(err != 0 && err != ypc__neterrcode(EINPROGRESS) && err != ypc__neterrcode(EWOULDBLOCK))
        return ypc__errcode(err);
    if(err == 0)
        y->state = y->tcps ? YPCN_TCPS : YPCN_CONNECTED;
    return 0;
}


static int ypc__write(ypc *y) {
    if(y->state == YPCN_CONNECTING)
        return ypc__connecting(y);

    ypc_iovec vec[16];
    int num = (y->tcps ? ypc__tcps_sendbuf : ypc__msgio_sendbuf)(y, vec, 16);
    ssize_t r;

#ifdef WINSOCK
    DWORD res = 0;
    r = WSASend(y->fd, vec, num, &res, 0, NULL, NULL);
    if(r == 0)
        r = res;
#else
    do {
        r = writev(y->fd, vec, num);
    } while(r < 0 && errno == EINTR);
#endif

    if(r < 0)
        return ypc__neterr(EAGAIN) || ypc__neterr(EWOULDBLOCK) ? 0 : ypc__errcode(0);

    (y->tcps ? ypc__tcps_sendlen : ypc__msgio_sendlen)(y, r);
    return 0;
}


static int ypc__read(ypc *y) {
    if(y->state == YPCN_CONNECTING) {
        /* We should not get a read event in the connecting state, but it's still
         * possible that this code is reached if ypc_process() is used in
         * non-blocking mode while the connection hasn't been established yet.
         * This is harmless, do nothing. */
        return 0;
    }

    uint8_t buf[YPC_READ_BUF_SIZE];
    ssize_t r;
    do {
        r = recv(y->fd, buf, sizeof(buf), 0);
    } while(r < 0 && ypc__neterr(EINTR));

    if(r < 0)
        return ypc__neterr(EAGAIN) || ypc__neterr(EWOULDBLOCK) ? 0 : ypc__errcode(0);
    if(r == 0)
        return YPC_EDISCONNECT;

    return (y->tcps ? ypc__tcps_recv : ypc__msgio_recv)(y, buf, r);
}


YPC_EXPORT int ypc_process(ypc *y, uint32_t flags) {
    int r = 0;
    while(r == 0) {
        /* Perform I/O */
        r = ypc__select(y, &flags);

        if(r == 0 && ypc_want_write(y) && (!(flags & (YPC_CANWRITE|YPC_CANREAD)) || flags & YPC_CANWRITE))
            r = ypc__write(y);

        if(r == 0 && (!(flags & (YPC_CANWRITE|YPC_CANREAD)) || flags & YPC_CANREAD))
            r = ypc__read(y);

        /* Figure out whether to return or to continue. */
        if(flags & YPC_NONBLOCK || y->recv.first)
            break;

        if(!(flags & YPC_RECV) && y->state == YPCN_CONNECTED && !y->send.first)
            break;

        flags &= ~(YPC_CANWRITE|YPC_CANREAD);
    }

    return r;
}

