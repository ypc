#include "../internal.h"

YPC_EXPORT int ypc_serv_unix(ypc_serv **sp, const char *path) {
    if(!sp)
        return YPC_EINVAL;

#ifdef WINSOCK
    return YPC_ENOSUPPORT;
#else

    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    int r = snprintf(addr.sun_path, sizeof(addr.sun_path), "%s", path);
    if(r < 0 || r >= (int)sizeof(addr.sun_path))
        return YPC_EADDR;

    ypc_serv *s = calloc(1, sizeof(ypc_serv));
    if(!s)
        return YPC_ENOMEM;

    r = ypc__serversock(AF_UNIX, &addr, sizeof(addr), false);
    if(r < 0) {
        free(s);
        return r;
    }

    s->fd = r;
    *sp = s;
    return 0;
#endif
}

