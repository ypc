#include "../internal.h"


/* Packet format:
 *   1 byte : length_prefix
 *   p bytes: packet data, p = (length_prefix+1)*16
 *
 * Packet data is between 16 and 4096 bytes long. There's three possible
 * layouts for the packet data:
 *
 * Client Hello (48 bytes):
 *    8 bytes: The string "YPCTCPS0"
 *    4 bytes: Client flags, currently always 0
 *    4 bytes: Reserved for future use, currently always 0
 *   32 bytes: client ephemeral key
 *
 * Server Hello (96 bytes):
 *   32 bytes: server public key
 *   16+48 bytes: crypto_box(msg, server public key -> client ephemeral key), msg:
 *      4 bytes: Copy of the client flags
 *      4 bytes: Server flags, currently always 0
 *      8 bytes: Reserved for future use, currently always 0
 *     32 bytes: server ephemeral key
 *
 * Server/Client Message (16+m bytes):
 *   crypto_box(msg, server <-> client ephemeral keys), msg:
 *     n         bytes: Message
 *     16-(n%16) bytes: PKCS7 padding
 *
 *   The initial 16 zero-bytes output of crypto_box are not sent.
 */

static void ypc__tcps_setlen(ypc_tcps *t, size_t len) {
    t->send[15] = (len/16)-1;
    t->slen = len+1;
    t->soff = 0;
}


int ypc__tcps_init(ypc *y) {
    ypc_tcps *t = malloc(sizeof(ypc_tcps));
    if(!t)
        return YPC_ENOMEM;

    t->slen = t->soff = t->rlen = t->roff = 0;
    memcpy(t->snonce,  y->server ? "YPC-SERVER-----M\0\0\0\0\0\0\0" : "YPC-CLIENT-----M\0\0\0\0\0\0\0", 24);
    memcpy(t->rnonce, !y->server ? "YPC-SERVER-----M\0\0\0\0\0\0\0" : "YPC-CLIENT-----M\0\0\0\0\0\0\0", 24);

    if(crypto_box_keypair(t->pk, t->sk) != 0) {
        free(t);
        return YPC_EFAIL;
    }

    if(!y->server) {
        /* Send Client Hello */
        memcpy(t->send+16, "YPCTCPS0", 8);
        memset(t->send+24, 0, 8);
        memcpy(t->send+32, t->pk, 48);
        ypc__tcps_setlen(t, 48);
    }
    y->tcps = t;
    return 0;
}


static void ypc__tcps_bumpnonce(uint8_t *nonce) {
    /* Increase the nonce number by 1 while maintaining network byte order.
     * I suspect this is faster than keeping an actual 64 bit variable and
     * swapping byte order every time. */
    size_t i = 23;
    while(++nonce[i] == 0)
        i--;
    /* This would mean we have sent over 2^64 packets. */
    assert(i >= 16);
}


static int ypc__tcps_clienthello(ypc *y) {
    ypc_tcps *t = y->tcps;
    assert(!t->slen);
    if(t->rlen < 48)
        return YPC_EPROTOCOL;

    uint8_t *recv = t->recv+16;
    if(memcmp(recv, "YPCTCPS0", 8) != 0)
        return YPC_EPROTOCOL;

    crypto_box_beforenm(t->nm, recv+16, t->sk);

    /* Send Server Hello */
    uint8_t msg[80];
    memcpy(msg+32, recv+8, 4);
    memset(msg+32+4, 0, 12);
    memcpy(msg+32+16, t->pk, 32);

    uint8_t *reply = t->send+16;
    uint8_t nonce[24] = {0};
    crypto_box(reply+16, msg, 80, nonce, recv+16, t->ssk);
    memcpy(reply, t->spk, 32);

    ypc__tcps_setlen(t, 96);
    y->state = YPCN_CONNECTED;
    return 0;
}


static int ypc__tcps_serverhello(ypc *y) {
    ypc_tcps *t = y->tcps;
    if(t->rlen < 96)
        return YPC_EPROTOCOL;

    uint8_t *recv = t->recv+16;
    if(memcmp(t->spk, recv, 32) != 0)
        return YPC_EPROTOCOL; /* Wrong public key. */

    uint8_t msg[80];
    uint8_t nonce[24] = {0};
    memset(recv+16, 0, 16);
    if(crypto_box_open(msg, recv+16, 80, nonce, t->spk, t->sk) != 0)
        return YPC_EPROTOCOL; /* Invalid signature */

    if(memcmp(msg+32, "\0\0\0", 4) != 0)
        return YPC_EPROTOCOL; /* Client flags aren't what we sent. */

    crypto_box_beforenm(t->nm, msg+32+16, t->sk);
    y->state = YPCN_CONNECTED;
    return 0;
}


static int ypc__tcps_packet(ypc *y) {
    if(y->state == YPCN_TCPS)
        return y->server ? ypc__tcps_clienthello(y) : ypc__tcps_serverhello(y);

    ypc_tcps *t = y->tcps;

    uint8_t msg[16+YPC_TCPS_MAXPACKET];
    memset(t->recv, 0, 16);
    size_t len = t->rlen+16;
    if(crypto_box_open_afternm(msg, t->recv, len, t->rnonce, t->nm) != 0)
        return YPC_EPROTOCOL;
    ypc__tcps_bumpnonce(t->rnonce);

    size_t nlen = len;
    size_t pad = msg[--nlen];
    if(!pad)
        return YPC_EPROTOCOL;
    /* crypto_box_open_afternm() guarantees that the first 32 bytes of the
     * output buffer are zeroed, which in turn prevents this loop from reading
     * before the start of the buffer. */
    while(nlen > len-pad)
        if(msg[--nlen] != pad)
            return YPC_EPROTOCOL;

    return ypc__msgio_recv(y, msg+32, nlen-32);
}


int ypc__tcps_recv(ypc *y, uint8_t *buf, size_t len) {
    ypc_tcps *t = y->tcps;

    while(len) {
        if(!t->rlen) {
            t->rlen = (*buf+1) * 16;
            len--;
            buf++;
            assert(t->rlen <= YPC_TCPS_MAXPACKET);
            continue;
        }

        size_t cpy = t->rlen - t->roff;
        if(cpy > len)
            cpy = len;
        memcpy(t->recv+16+t->roff, buf, cpy);

        t->roff += cpy;
        len -= cpy;
        buf += cpy;

        if(t->roff == t->rlen) {
            int r = ypc__tcps_packet(y);
            if(r < 0)
                return r;
            t->roff = t->rlen = 0;
        }
    }
    return 0;
}


static void ypc__tcps_encrypt(ypc *y) {
    ypc_tcps *t = y->tcps;

    uint8_t msg[16+YPC_TCPS_MAXPACKET];
    memset(msg, 0, 32);

    ypc_iovec vec[16];
    int n = ypc__msgio_sendbuf(y, vec, 16);
    int i = 0;
    size_t len = 32;
    while(i < n && len < sizeof(msg)-1) {
        size_t cpy = sizeof(msg) - 1 - len;
        if(cpy > ypc__iovec_len(vec[i]))
            cpy = ypc__iovec_len(vec[i]);
        memcpy(msg+len, ypc__iovec_buf(vec[i]), cpy);
        i++;
        len += cpy;
    }
    ypc__msgio_sendlen(y, len-32);

    size_t pad = 16 - (len & 15);
    memset(msg+len, pad, pad);
    len += pad;

    crypto_box_afternm(t->send, msg, len, t->snonce, t->nm);
    ypc__tcps_bumpnonce(t->snonce);
    ypc__tcps_setlen(t, len-16);
}


int ypc__tcps_sendbuf(ypc *y, ypc_iovec *vec, int max) {
    ypc_tcps *t = y->tcps;

    if(y->state == YPCN_CONNECTED && t->slen == t->soff) {
        assert(y->send.first);
        ypc__tcps_encrypt(y);
    }
    assert(t->slen && t->slen != t->soff);
    ypc__iovec_buf(vec[0]) = t->send + 15 + t->soff;
    ypc__iovec_len(vec[0]) = t->slen - t->soff;
    return 1;
}


void ypc__tcps_sendlen(ypc *y, size_t len) {
    ypc_tcps *t = y->tcps;

    assert(t->slen != t->soff);
    assert(t->slen - t->soff <= len);
    t->soff += len;
}
