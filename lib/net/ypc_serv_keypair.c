#include "../internal.h"

YPC_EXPORT int ypc_serv_keypair(char *pub, size_t publen, char *sec, size_t seclen) {
    uint8_t pk[32];
    uint8_t sk[32];

    if(publen < 65 || seclen < 65)
        return YPC_EINVAL;
    if(crypto_box_keypair(pk, sk) != 0)
        return YPC_EFAIL;

    ypc__bin2hex(pub, pk, 32);
    ypc__bin2hex(sec, sk, 32);
    return 0;
}

