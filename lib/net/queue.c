#include "../internal.h"

ypc_msg *ypc__dequeue(ypc_queue *queue) {
    if(queue->first == NULL)
        return NULL;

    ypc_msg *item = queue->first;
    queue->first = item->next;
    if(item->next == NULL)
        queue->last = NULL;
    return item;
}


void ypc__enqueue(ypc_queue *queue, ypc_msg *item) {
    item->next = NULL;
    if(queue->last)
        queue->last->next = item;
    else
        queue->first = item;
    queue->last = item;
}
