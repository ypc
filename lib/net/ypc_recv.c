#include "../internal.h"

YPC_EXPORT int ypc_recv(ypc *y, ypc_val *v) {
    ypc_msg *m = ypc__dequeue(&y->recv);
    if(!m)
        return 0;
    *v = m->v;
    free(m);
    return 1;
}
