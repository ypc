#include "../internal.h"
#include "../util/str2sockaddr.h"

static socklen_t ypc__aflen(int af) {
    switch(af) {
    case AF_INET:  return sizeof(struct sockaddr_in);
    case AF_INET6: return sizeof(struct sockaddr_in6);
#ifndef WINSOCK
    case AF_UNIX:  return sizeof(struct sockaddr_un);
#endif
    }
    return 0;
}


static int ypc__connect(const char *addr, const char **key) {
    ypc__saddr s;
    if(ypc__str2sockaddr(addr, &s, key) < 0)
        return YPC_EADDR;

    int fd = ypc__socket(s.addr.sa_family, SOCK_STREAM, 0);
    if(fd < 0)
        return fd;

    /* Initiate a non-blocking connect.
     * It's possible that the connect succeeds immediately, but it's safe to
     * handle that the same way as an EINPROGRESS. */
    int r = connect(fd, &s.addr, ypc__aflen(s.addr.sa_family));
    if(r < 0 && !ypc__neterr(EINPROGRESS) && !ypc__neterr(EWOULDBLOCK)) {
        r = ypc__errcode(0);
        close(fd);
        return r;
    }

    return fd;
}


YPC_EXPORT int ypc_connect(ypc **yp, const char *addr) {
    ypc *y = calloc(1, sizeof(ypc));
    if(!y)
        return YPC_ENOMEM;

    const char *key = NULL;
    int r = ypc__connect(addr, &key);
    if(r < 0) {
        free(y);
        return r;
    }
    y->fd = r;

    y->state = YPCN_CONNECTING;
    if(key) {
        uint8_t pubkey[32];
        if(ypc__hex2bin(pubkey, key, 32) != 0)
            return YPC_EADDR;

        r = ypc__tcps_init(y);
        if(r < 0) {
            close(y->fd);
            free(y);
            return r;
        }

        memcpy(y->tcps->spk, pubkey, 32);
    }

    *yp = y;
    return 0;
}

