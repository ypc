#include "../internal.h"

YPC_EXPORT int ypc_serv_tcps(ypc_serv **sp, const char *addr, const char *pub, const char *sec) {
    if(!sp)
        return YPC_EINVAL;

    uint8_t ssk[32];
    uint8_t spk[32];

    if(ypc__hex2bin(ssk, sec, 32) != 0)
        return YPC_EINVAL;
    if(ypc__hex2bin(spk, pub, 32) != 0)
        return YPC_EINVAL;

    ypc_serv *s;
    int r = ypc_serv_tcp(&s, addr);
    if(r < 0)
        return r;

    s->secure = true;
    memcpy(s->ssk, ssk, 32);
    memcpy(s->spk, spk, 32);
    *sp = s;
    return 0;
}

