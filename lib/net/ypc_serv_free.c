#include "../internal.h"

YPC_EXPORT void ypc_serv_free(ypc_serv *s) {
#ifndef WINSOCK
    struct sockaddr_un un;
    socklen_t unlen = sizeof(un);
    if(getpeername(s->fd, (void*)&un, &unlen) == 0 && un.sun_family == AF_UNIX
            && unlen > (socklen_t)sizeof(sa_family_t)+1 && *un.sun_path)
        unlink(un.sun_path);
#endif

    close(s->fd);
    free(s);
}
