#include "../internal.h"

YPC_EXPORT void ypc_free(ypc *y) {
    if(!y)
        return;

    ypc_msg *m;
    while((m = ypc__dequeue(&y->recv)) != NULL) {
        ypc_val_free(&m->v);
        free(m);
    }
    while((m = ypc__dequeue(&y->send)) != NULL) {
        ypc_val_free(&m->v);
        free(m);
    }
    ypc_val_free(&y->recv_val);

    close(y->fd);
    free(y->tcps);
    free(y);
}

