#include "../internal.h"
#include "../util/str2sockaddr.h"

static int ypc__parseaddr(const char *addr, ypc__saddr *sa, bool *iponly) {
    *iponly = false;

    /* ip:port */
    if(ypc__str2sockaddr_ip(&addr, sa) == 0) {
        if(*addr != ':')
            return YPC_EADDR;
        addr++;
        if(ypc__str2sockaddr_port(&addr, sa) != 0)
            return YPC_EADDR;
        /* just a port */
    } else {
        if(*addr == ':')
            addr++;
        if(ypc__str2sockaddr_port(&addr, sa) != 0)
            return YPC_EADDR;
        *iponly = true;
    }
    /* No garbage after the address, please. */
    if(*addr)
        return YPC_EADDR;

    return 0;
}


static int ypc__tcpsock(ypc__saddr *sa, bool trydual) {
    int fd = ypc__serversock(sa->addr.sa_family, sa,
            sa->addr.sa_family == AF_INET ? sizeof(sa->ip4) : sizeof(sa->ip6), trydual);

    /* If dual stack didn't work, fall back to IPv4 */
#ifdef IPV6_V6ONLY
    if(fd < 0 && trydual) {
        struct sockaddr_in sa4 = {0,};
        sa4.sin_port = sa->ip6.sin6_port;
        fd = ypc__serversock(AF_INET, &sa4, sizeof(sa4), false);
    }
#endif

    return fd;
}


YPC_EXPORT int ypc_serv_tcp(ypc_serv **sp, const char *addr) {
    if(!sp)
        return YPC_EINVAL;

    ypc__saddr sa = {{0,}};
    sa.addr.sa_family = AF_INET6;
    bool trydual;

    int r = ypc__parseaddr(addr, &sa, &trydual);
    if(r < 0)
        return r;

    ypc_serv *s = calloc(1, sizeof(ypc_serv));
    if(!s)
        return YPC_ENOMEM;

    r = ypc__tcpsock(&sa, trydual);
    if(r < 0) {
        free(s);
        return r;
    }

    s->fd = r;
    *sp = s;
    return 0;
}
