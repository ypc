#include "../internal.h"

YPC_EXPORT int ypc_send(ypc *y, ypc_val *v, uint32_t flags) {
    /* XXX: Honor COPY flag */
    ypc_msg *m = malloc(sizeof(ypc_msg));
    if(!m)
        return -1;
    m->v = *v;
    ypc__enqueue(&y->send, m);
    return 0;
}
