#include "../internal.h"

static int ypc__block(ypc_serv *s) {
    fd_set rd;
    FD_ZERO(&rd);
    FD_SET(s->fd, &rd);

    int r;
    do {
        r = select(s->fd+1, &rd, NULL, NULL, NULL);
    } while(r < 0 && ypc__neterr(EINTR));
    return r < 0 ? ypc__errcode(0) : 0;
}


YPC_EXPORT int ypc_serv_accept(ypc_serv *s, ypc **yp, uint32_t flags) {
    int r;
    if(!(flags & YPC_NONBLOCK)) {
        r = ypc__block(s);
        if(r < 0)
            return r;
    }

    do {
        r = accept(s->fd, NULL, NULL);
    } while(r < 0 && ypc__neterr(EINTR));

    if(r < 0)
        return ypc__errcode(0);

    ypc *y = calloc(1, sizeof(ypc));
    if(!y) {
        close(r);
        return YPC_ENOMEM;
    }
    y->fd = r;
    y->server = true;

    y->state = s->secure ? YPCN_TCPS : YPCN_CONNECTED;
    if(s->secure) {
        r = ypc__tcps_init(y);
        if(r < 0) {
            close(y->fd);
            free(y);
            return r;
        }
        memcpy(y->tcps->ssk, s->ssk, 32);
        memcpy(y->tcps->spk, s->spk, 32);
    }
    *yp = y;
    return 0;
}
