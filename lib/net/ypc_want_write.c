#include "../internal.h"

#define tcps_buffered(y) (y->tcps && (y)->tcps->slen != (y)->tcps->soff && (y)->tcps->slen)

YPC_EXPORT bool ypc_want_write(ypc *y) {
    switch((int)y->state) {
    case YPCN_CONNECTING:
        return true;
    case YPCN_TCPS:
        return tcps_buffered(y);
    case YPCN_CONNECTED:
        return y->send.first || tcps_buffered(y);
    }
    return false;
}
