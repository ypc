#include "../internal.h"

#define YPC_MAX_MSG_SIZE (8*1024*1024)

int ypc__msgio_recv(ypc *y, uint8_t *buf, size_t len) {
    while(len) {
        if(!y->recv_val.len) {
            if(ypc_val_init(&y->recv_val, 1024) != 0)
                return YPC_ENOMEM;
            ypc_val_parse_init(&y->recv_parse);
        }

        size_t l = len;
        int r = ypc_val_parse(&y->recv_parse, buf, &l);
        if(r < 0 || y->recv_val.len + l > YPC_MAX_MSG_SIZE)
            return YPC_EPROTOCOL;

        if(ypc_add_raw(&y->recv_val, buf, l) != 0)
            return YPC_ENOMEM;

        /* Complete message */
        if(r == 1) {
            ypc_msg *m = malloc(sizeof(ypc_msg));
            if(!m)
                return YPC_ENOMEM;
            m->v = y->recv_val;
            ypc__enqueue(&y->recv, m);

            memset(&y->recv_val, 0, sizeof(y->recv_val));
        }

        buf += l;
        len -= l;
    }

    return 0;
}


int ypc__msgio_sendbuf(ypc *y, ypc_iovec *vec, int max) {
    assert(y->send.first);
    size_t off = y->send_off;
    ypc_msg *m = y->send.first;
    int n = 0;
    while(m && n < max) {
        ypc__iovec_buf(vec[n]) = m->v.buf + off;
        ypc__iovec_len(vec[n]) = m->v.len - off;
        off = 0;

        n++;
        m = m->next;
    }
    return n;
}


void ypc__msgio_sendlen(ypc *y, size_t len) {
    while(len) {
        ypc_msg *m = y->send.first;
        size_t left = m->v.len - y->send_off;
        if(left > len) {
            y->send_off += len;
            break;
        }
        ypc__dequeue(&y->send);
        ypc_val_free(&m->v);
        free(m);
        y->send_off = 0;
        len -= left;
    }
}

