#include "../lib/ypc.h"
#include "../lib/util/bin2hex.c"
#include <math.h>
#include <stdio.h>

#ifdef NDEBUG
#undef NDEBUG
#endif
#include <assert.h>


#define T_MSG\
    C("\x0a",                               ypc_add_array(v))\
    C("\x00",                                 ypc_add_null(v))\
    C("\x02",                                 ypc_add_bool(v, true))\
    C("\x01",                                 ypc_add_bool(v, false))\
    C("\x03\x01",                             ypc_add_int(v, 1))\
    C("\x03\xff",                             ypc_add_int(v, -1))\
    C("\x04\x01\xf4",                         ypc_add_int(v, 500))\
    C("\x04\xfe\x0c",                         ypc_add_int(v, -500))\
    C("\x05\x00\x07\xa1\x20",                 ypc_add_int(v, 500000))\
    C("\x05\xff\xf8\x5e\xe0",                 ypc_add_int(v, -500000))\
    C("\x06\x7f\xff\xff\xff\xff\xff\xff\xff", ypc_add_int(v, INT64_MAX))\
    C("\x06\x80\x00\x00\x00\x00\x00\x00\x00", ypc_add_int(v, INT64_MIN))\
    C("\x07\x00\x00\x00\x00\x00\x00\x00\x00", ypc_add_float(v, 0.0))\
    C("\x07\x40\x09\x21\xfb\x54\x44\x2d\x18", ypc_add_float(v, M_PI))\
    C("\x07\x7f\xf0\x00\x00\x00\x00\x00\x00", ypc_add_float(v, HUGE_VAL))\
    C("\x0b",                                 ypc_add_map(v))\
    C("\x0c",                                 ypc_add_close(v))\
    C("\x0b",                                 ypc_add_map(v))\
    C("\x09""abc\0",                            ypc_add_text(v, "abc"))\
    C("\x09""foob\0",                           ypc_add_textn(v, "foobar", 4))\
    C("\x08\0\0\5\0\1\2\3\xff",                 ypc_add_bin(v, (const uint8_t *)"\0\1\2\3\xff", 5))\
    C("\x08\0\0\0",                             ypc_add_bin(v, (const uint8_t *)"", 0))\
    C("\x0c",                                 ypc_add_close(v))\
    C("\x0c",                               ypc_add_close(v))

#define C(s, x) s
static const char msg[] = T_MSG;
#undef C

#define STR(x) #x

static void t_create(ypc_val *v) {
    assert(ypc_val_init(v, 32) == 0);
#define C(s, x) do {\
        size_t l = v->len;\
        if(x != 0) {\
            fputs(STR(x)" failed\n", stderr);\
            exit(1);\
        }\
        l = v->len - l;\
        if(l != sizeof s - 1 || memcmp(v->buf + v->len - l, s, l) != 0) {\
            char exp[2*sizeof s];\
            ypc__bin2hex(exp, (const uint8_t *)s, sizeof s - 1);\
            char got[2*l+1];\
            ypc__bin2hex(got, v->buf + v->len - l, l);\
            fprintf(stderr, "%s produced wrong output\n  Got: %s\n  Expected: %s\n", STR(x), got, exp);\
            exit(1);\
        }\
    } while(0);
    T_MSG
#undef C

    assert(v->len == sizeof msg - 1);
    assert(memcmp(v->buf, msg, v->len) == 0);
}


static void t_read(ypc_get g) {
    assert(ypc_get_type(g) == YPC_ARRAY);
    assert(ypc_get_size(g) == sizeof msg - 1);
    ypc_iter i = ypc_get_iter(g);
    ypc_iter m;

#define V(i, t, x) do {\
        assert(!ypc_iter_end(i));\
        g = ypc_iter_next(&i);\
        assert(ypc_get_type(g) == t);\
        assert(x);\
    } while(0)

    V(i, YPC_NULL, 1);
    V(i, YPC_BOOL, ypc_get_bool(g) == true);
    V(i, YPC_BOOL, ypc_get_bool(g) == false);
    V(i, YPC_INT, ypc_get_int(g) == 1);
    V(i, YPC_INT, ypc_get_int(g) == -1);
    V(i, YPC_INT, ypc_get_int(g) == 500);
    V(i, YPC_INT, ypc_get_int(g) == -500);
    V(i, YPC_INT, ypc_get_int(g) == 500000);
    V(i, YPC_INT, ypc_get_int(g) == -500000);
    V(i, YPC_INT, ypc_get_int(g) == INT64_MAX);
    V(i, YPC_INT, ypc_get_int(g) == INT64_MIN);
    V(i, YPC_FLOAT, ypc_get_float(g) == 0.0);
    V(i, YPC_FLOAT, ypc_get_float(g) == M_PI);
    V(i, YPC_FLOAT, ypc_get_float(g) == HUGE_VAL);

    V(i, YPC_MAP, ypc_iter_end(ypc_get_iter(g)));

    V(i, YPC_MAP, 1);
    m = ypc_get_iter(g);
    V(m, YPC_TEXT, strcmp(ypc_get_text(g), "abc") == 0);
    V(m, YPC_TEXT, strcmp(ypc_get_text(g), "foob") == 0);
    V(m, YPC_BIN, ypc_get_bin_len(g) == 5 && memcmp(ypc_get_bin(g), "\0\1\2\3\xff", 5) == 0);
    V(m, YPC_BIN, ypc_get_bin_len(g) == 0);
    assert(ypc_iter_end(m));
#undef V

    assert(ypc_iter_end(i));
}


int main(int argc, char **argv) {
    ypc_val v;
    t_create(&v);
    t_read(ypc_val_get(&v));
    ypc_val_free(&v);
    return 0;
}
