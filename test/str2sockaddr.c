#include "../lib/internal.h"
#include "../lib/util/str2sockaddr.h"

#ifdef NDEBUG
#undef NDEBUG
#endif
#include <assert.h>


static void t_port() {
#define E(str) do {\
        ypc__saddr a;\
        const char *s = str;\
        const char *o = s;\
        assert(ypc__str2sockaddr_port(&s, &a) < 0);\
        assert(s == o);\
    } while(0)
    E("");
    E("0");
    E("0123");
    E("x");
    E(":1");
    E("65536");
    E("-1");
#undef E

#define O(str, len, val) do {\
        ypc__saddr a;\
        a.addr.sa_family = AF_INET;\
        const char *s = str;\
        const char *o = s;\
        assert(ypc__str2sockaddr_port(&s, &a) == 0);\
        assert(s == o+len);\
        assert(a.ip4.sin_port == htons(val));\
    } while(0)
    O("1", 1, 1);
    O("65535", 5, 65535);
    O("1//", 1, 1);
    O("1ABC", 1, 1);
    O("99X", 2, 99);
    O("345X", 3, 345);
    O("4567X", 4, 4567);
    O("30000X", 5, 30000);
#undef O
}


static void t_ip() {
#define E(str) do {\
        ypc__saddr a;\
        const char *s = str;\
        const char *o = s;\
        assert(ypc__str2sockaddr_ip(&s, &a) < 0);\
        assert(s == o);\
    } while(0)
    E("");
    E("x");
    E("/127.0.0.1");
    E("10.0.0.");
    /* Apparently, this is a valid IP address to cygwin
    E("0.01.0.0"); */
    E("10");
    E("127.0.256.1");
    E("1.2.3..1");
    E("1.2.3.x");
    E("[127.0.0.1]");
    E("[FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF]");
    E("[::0");
#undef E

    /* XXX: Should also verify the converted address */
#define O(str, len) do {\
        ypc__saddr a;\
        const char *s = str;\
        const char *o = s;\
        assert(ypc__str2sockaddr_ip(&s, &a) == 0);\
        assert(s == o+len);\
    } while(0)
    O("10.0.0.1", 8);
    O("10.0.0.1:124", 8);
    O("10.0.0.1XYZ", 8);
    O("255.255.255.255", 15);
    O("[::]", 4);
    O("[::1]", 5);
    O("[::1]:124", 5);
    O("[::1]XYZ", 5);
#undef O
}


void t_addr() {
#define E(str, e) do {\
        ypc__saddr a;\
        const char *k = NULL;\
        assert(ypc__str2sockaddr(str, &a, &k) == e);\
        assert(k == NULL);\
    } while(0)
    E("", YPC_EADDR);
    E("unix", YPC_EADDR);
    E("10.0.0.1", YPC_EADDR);
    E("10.0.0.1:1234", YPC_EADDR);
    E("[0.0.0.0]:1234", YPC_EADDR);
    E("abc://", YPC_EADDR);
    E("tcp://", YPC_EADDR);
#ifdef WINSOCK
    E("unix:/some/path", YPC_ENOSUPPORT);
#else
    E("unix:/this/path/exceeds/UNIX_PATH_MAX/and/should/therefore/not/be/allowed/as/a/url/or/anything/that/one/would/connect/to/normally", YPC_EADDR);
    E("unix:", YPC_EADDR);
#endif
    E("tcp://127.0.0.1", YPC_EADDR);
    E("tcp://127.0.0.1/", YPC_EADDR);
    E("tcp://127.0.0.1:0", YPC_EADDR);
    E("tcp://127.0.0.1:02", YPC_EADDR);
    E("tcp://127.0.0.1:65536", YPC_EADDR);
    E("tcp://127.0.0.1:1234/garbage", YPC_EADDR);
    E("tcp://[::1]/garbage", YPC_EADDR);
    E("tcp://[::1]:1234/garbage", YPC_EADDR);
    E("tcps://[::1]:1234", YPC_EADDR);
#undef E

#define O(str, af, key) do {\
        ypc__saddr a;\
        const char *k = NULL;\
        assert(ypc__str2sockaddr(str, &a, &k) == 0);\
        assert(a.addr.sa_family == af);\
        if(key) assert(strcmp(k, key ? key : "") == 0);\
        else    assert(k == NULL);\
    } while(0)
    O("tcp://10.0.0.1:1/", AF_INET, NULL);
    O("tcp://10.0.0.1:1234/", AF_INET, NULL);
    O("tcp://127.0.0.1:1234", AF_INET, NULL);
    O("tcps://127.0.0.1:1234/abc", AF_INET, "abc"); /* Validating key isn't part of ypc__str2sockaddr() */
    O("tcp://[::]:1234", AF_INET6, NULL);
    O("tcps://[::]:1234/abc", AF_INET6, "abc");
#ifndef WINSOCK
    O("unix:/some/path", AF_UNIX, NULL);
#endif
#undef O
}


int main(int argc, char **argv) {
    t_port();
    t_ip();
    t_addr();
    return 0;
}

