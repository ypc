#include "../lib/internal.h"

#ifdef NDEBUG
#undef NDEBUG
#endif
#include <assert.h>

int main(int argc, char **argv) {
#define E(str, len) do {\
        uint8_t buf[len];\
        assert(ypc__hex2bin(buf, str, len) < 0);\
    } while(0)
    E("", 1);
    E("A", 1);
    E("AA", 2);
    E("X", 1);
    E("X0", 1);
    E("/0", 1);
    E("0X", 1);
    E("0/", 1);
    E("000", 1);
    E("0000", 1);
#undef E

#define T(hex, bin, len) do {\
        uint8_t buf[len];\
        char buf2[len*2+1];\
        assert(ypc__hex2bin(buf, hex, len) == 0);\
        assert(memcmp(buf, bin, len) == 0);\
        ypc__bin2hex(buf2, (const uint8_t *)bin, len);\
        assert(buf2[len*2] == 0);\
        assert(strcasecmp(buf2, hex) == 0);\
    } while(0)
    T("", "", 0);
    T("00", "\0", 1);
    T("FF", "\377", 1);
    T("ff", "\377", 1);
    T("fF", "\377", 1);
    T("FF00", "\377\0", 2);
    T("000102030405060708090A0B0C0D0E0F", "\0\1\2\3\4\5\6\7\10\11\12\13\14\15\16\17", 16);
    T("102030405060708090A0B0C0D0E0F0", "\20\40\60\100\120\140\160\200\220\240\260\300\320\340\360", 15);
#undef T

    return 0;
}
