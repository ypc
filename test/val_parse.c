#include <assert.h>
#include "../lib/ypc.h"

#define T(str, ret) do {\
        /* one-shot validation */\
        ypc_val_parser p[1];\
        ypc_val_parse_init(p);\
        size_t l = sizeof str - 1;\
        const uint8_t *buf = (const uint8_t *)str;\
        assert(1 && ypc_val_parse(p, buf, &l) == ret);\
        if(ret == 1)\
            assert(1 && l == sizeof str - 1);\
        /* one-shot validation with trailing garbage */\
        const uint8_t bin[] = str "garbage";\
        l = sizeof bin - 1;\
        assert(2 && ypc_val_parse(p, bin, &l) == ret);\
        if(ret == 1)\
            assert(2 && l == sizeof str - 1);\
        /* one byte at a time */\
        ypc_val_parse_init(p);\
        int r = 0;\
        size_t i;\
        l = 0;\
        while(r == 0) {\
            i = 1;\
            r = ypc_val_parse(p, buf, &i);\
            assert(i == 1);\
            l++;\
            buf++;\
        }\
        assert(r == ret);\
        if(ret == 1)\
            assert(l == sizeof str - 1);\
    } while(0)

int main(int argc, char **argv) {
    assert(sizeof(ypc_val_parser) == 16);
    /* null */
    T("\0", 1);
    /* false */
    T("\1", 1);
    /* true */
    T("\2", 1);
    /* int8, 0 */
    T("\3\0", 1);
    /* int16, 0 */
    T("\4\0\0", 1);
    /* int32, 0 */
    T("\5\0\0\0\0", 1);
    /* int64, 0 */
    T("\6\0\0\0\0\0\0\0\0", 1);
    /* float, 0 */
    T("\7\0\0\0\0\0\0\0\0", 1);
    /* binary stuff (TODO: Test longer strings) */
    T("\10\0\0\0", 1);
    T("\10\0\0\5\1\2\3\4\5", 1);
    /* text strings */
    T("\11\0", 1);
    T("\11Hey\0", 1);
    T("\11オリジナルサウンドトラック\0", 1);
    T("\11\xC2\xA2 \xE2\x82\xAC \xF0\xA4\xAD\xA2\0", 1); /* UTF-8 examples from Wikipedia */
    T("\11\xC2\0", -1);
    T("\11\xC0\x80\0", -1);
    T("\11\xF0\x82\xAC\0", -1); /* Overlong (Example from wikipedia) */
    /* Arrays */
    T("\12\0\14", 1);
    T("\12\0\1\11abc\0\14", 1);
    T("\12\12\12\14\14\14", 1);
    T("\14", -1);
    /* Maximum nesting is 32 */
    T("\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14\14", 1);
    T("\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12\12", -1);
    /* Maps */
    T("\13\14", 1);
    T("\13\0\14", -1);
    T("\13\0\0\14", -1);
    T("\13\3\0\0\14", 1);
    T("\13\10\0\0\0\10\0\0\1\1\14", 1);
    T("\13\3\0\0\3\1\0\14", 1);
    T("\12\13\14\14", 1);
    return 0;
}
