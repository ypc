=head1 Overview

TODO.


=head1 API

=head2 General Stuff

=over

=item int ypc_init()

Global initialization function. This function does the following:

=over

=item * On windows, calls WSAStartup()

=item * When using libsodium, calls sodium_init()

=back

Returns 0 on success or YPC_EFAIL on failure.

This function does not need to be called if you have already done the necessary
libsodium or winsock initialization yourself, or when you know you're using
neither libsodium nor Windows. In all other cases, make sure to call this
function before using any other ypc_* functionality. It is safe to call this
function more than once. After this function has completed at least once, it
can be called from multiple threads, too.

=back


=head2 Message Handling

=over

=item void ypc_val_parse_init(ypc_val_parser *p)

Initialize a value parsing object for use with ypc_val_parse().

=item int ypc_val_parse(ypc_val_parser *p, const uint8_t *buf, size_t *len)

Parse (or, actually, validate) a serialized value. Returns -1 on error, 0 if
the buffer parsed correctly, but does not finish a complete value, and 1 if the
buffer completes a value. C<len> is updated to reflect the number of bytes that
have been verified in C<buf>. On error, this indicates the offset into the
buffer where the error occured, and on a complete value this indicates the end
of the value.

=item ypc_msg *ypc_msg_new(const char *, size_t)

=item void ypc_msg_free(ypc_msg *)

=back


=head2 Connection Object

=over

=item int ypc_connect(ypc **, const char *addr)

Create a new ypc client object. Doesn't actually do any network I/O, this is
done in C<ypc_process()>. I<addr> is in either of the following two formats:

  unix:/path
  tcp://ip:port/
  tcps://ip:port/publickey

Although most connection errors are deferred to C<ypc_process()>, it is still
possible that this function fails immediately with a connection error
(especially true for UNIX sockets).

=item int ypc_process(ypc *, uint32_t flags);

Process any queued events (if any) and read new messages from the network.
Accepted flags:

  YPC_NONBLOCK: Do not block on an underlying socket action. The application
    should poll for I/O readiness and call this function again.
  YPC_RECV: Keep blocking until at least one message has been received.
    Conflicts with YPC_NONBLOCK.
  YPC_CANREAD & YPC_CANWRITE: Whether a poll() has previously indicated
    if a read or write is possible without blocking. If neither flag is
    given, ypc_process() will try to read or write as necessary, but this
    may involve an extra system call.

This function returns when any of the following is true:

=over

=item * A new message has been received.

=item * The connection has been established and all outstanding writes have
been flushed (unless YPC_RECV is set).

=item * YPC_NONBLOCK is set and a socket action would block.

=item * Some network error.

=back

=item bool ypc_want_write(ypc *)

Whether ypc_process() has something to write to the fd (i.e. whether, in
non-blocking mode, the application should poll for writing).  Applications
should always poll for reading.

=item int ypc_fd(ypc *)

Returns the socket fd for polling.

=item int ypc_send(ypc *, ypc_msg *, uint32_t flags)

Queue a message for sending. Won't actually be sent until ypc_process() is
called. Function takes ownership of the message and will free it when the
message has been sent. Flags:

  YPC_COPY: Create an internal copy of the message. The given message will
    not be freed and ownership remains to the application.
    (Note: The message structs don't need to be copied verbatim, message can
    be serialized directly as part of this function call)

Returns an error if the send queue is full (size is configurable).  Also
returns an error if the ypc object is not connected anymore?

  (Annoying caveat: If an error is returned, then the given message should
  not be freed. If an application assumes that this function will not fail
  and forgets to free the message on error, there will be a a memory leak)

XXX: This is a low-level interface, a higher-level API is necessary for RPC
calls, in order to keep state for tracking the associated reply.


=item ypc_msg *ypc_recv(ypc *)

Get the next message on the receive queue. As with ypc_send(), doesn't actually
do any network I/O. Only messages that have been received in a ypc_process()
call will be returned. Returned message should be freed by the application.
Returns NULL if the receive queue is empty.

=item void ypc_free(ypc *)

Discards and frees any queued actions, closes socket, and frees ypc object.

=back


=head2 Server Connections

=over

=item int ypc_serv_unix(ypc_serv **, const char *)

=item int ypc_serv_tcp(ypc_serv **, const char *)

Address can be "port", ":port" or "host:port".

=item int ypc_serv_keypair(char *, size_t, char *, size_t)

=item int ypc_serv_tcps(ypc_serv **, const char *, const char *, const char *)

=item int ypc_serv_accept(ypc_serv *, ypc **, uint32_t)

Only flag is YPC_NONBLOCK.

=item int ypc_serv_fd(ypc_serv *)

=item void ypc_serv_free(ypc_serv *)

Stop listening for new connections and free object.

=back


=head1 Protocol

TODO.
